﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data.Linq;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        string[] query;
        PlaylistManager DB;
        string CurPlaylist;
        public Window1(string name)
        {
            query = new string[4] {"","","",""};
            CurPlaylist = name;
            InitializeComponent();
        }

        private MainWindow playerData;


        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox tb = sender as TextBox;
            String text = tb.Text;
            if (String.IsNullOrWhiteSpace(text) == false)
                query[Convert.ToInt32(tb.Uid)] = text;
            else
                query[Convert.ToInt32(tb.Uid)] = "";
            updateResult();
        }

        private void updateResult()
        {
            var result = from Songs in DB.Playlist
                         where Songs.PlaylistName == CurPlaylist
                         where Songs.Title.Contains(query[0])
                         where Songs.Artist.Contains(query[1])
                         where Songs.Album.Contains(query[2])
                         where Songs.Genre.Contains(query[3])
                         select Songs.Title;
            foreach (string song in result)
            {
                Result.Items.Add(song);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }
       
    }
}

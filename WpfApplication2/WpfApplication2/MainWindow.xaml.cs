﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Linq;

namespace WpfApplication2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //Console.Beep();
            //PlaylistDB = new PlaylistManager(".\\PlayListDataBase.mdf");
           // if (PlaylistDB == null)
             //   Console.Beep();
           /* if (PlaylistDB.DatabaseExists())
            {
                Console.Beep();
                PlaylistDB.DeleteDatabase();
            }*/

            //CreateDatabase2();
                
            //Console.Beep();
            //PlaylistDB.CreateDatabase();
            //Console.Beep();
            this.playlist.Visibility = Visibility.Hidden;
            //loadMedia("toto");
            MediaPlayer.MediaOpened += new RoutedEventHandler(MediaOpened);
            //playlistWindow = new Window1(this);
            // MediaPlayer.Play();
        }

        public void CreateDatabase2()
        {
            PlaylistDB = new PlaylistManager(@"c:\PlayListDataBase.mdf");
            if (PlaylistDB.DatabaseExists())
            {
                Console.WriteLine("Deleting old database...");
                PlaylistDB.DeleteDatabase();
            }
            PlaylistDB.CreateDatabase();
        }

        PlaylistManager PlaylistDB;
        private void loadMedia(string url)
        {
            MediaPlayer.MediaOpened += new RoutedEventHandler(MediaOpened);
            MediaPlayer.Source = new Uri(url);
        }

        private void MediaOpened(object sender, RoutedEventArgs e)
        {
            mediaLength = MediaPlayer.NaturalDuration.TimeSpan.TotalMilliseconds;
            TimeSlider.Maximum = mediaLength;
            TimeSlider.SmallChange = mediaLength / 1000;
            TimeSlider.LargeChange = mediaLength / 100;
            MediaPlayer.Play();
        }

        private void ChangeMediaVolume(object sender, RoutedPropertyChangedEventArgs<double> args)
        {
            MediaPlayer.Volume = (double)soundSlider.Value;
        }

        private void pauseButton_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Pause();
        }

        private void buttonPlay_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Play();
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            MediaPlayer.Stop();
        }

        private void TimeSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            double SliderValue = TimeSlider.Value;

            TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)(SliderValue));
            MediaPlayer.Position = ts;
        }

        private void File_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog opnDialogue = new System.Windows.Forms.OpenFileDialog();
            opnDialogue.Filter = "Video Files(*.wmv)|*.wmv|Video Files(*.avi)|*.avi|Video Files(*.mkv)|*.mkv|All files(*.*)|*.*";
            if (opnDialogue.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.MediaPlayer.Source = new Uri(opnDialogue.FileName);
            }
        }

        private void Playlist_Click(object sender, RoutedEventArgs e)
        {
            this.playlist.Visibility = Visibility.Visible;            
        }

        private void newPLaylist_Click(object sender, RoutedEventArgs e)
        {
            playlistList.Items.Add(newName.Text);
        }

        private Window1 playlistWindow;
        public double mediaLength;

        private void filter_Click(object sender, RoutedEventArgs e)
        {

         // string target = playlistList.SelectedItem.ToString();
            Window1 filterWindow = new Window1("");
              
            filterWindow.Show();

        }

        private void addSong_Click(object sender, RoutedEventArgs e)
        {

            /*
             string PlaylistName = playlistList.SelectedItem.ToString();
            System.Windows.Forms.OpenFileDialog opnDialogue = new System.Windows.Forms.OpenFileDialog();
            opnDialogue.Filter = "Video Files(*.wmv)|*.wmv|Video Files(*.avi)|*.avi|Video Files(*.mkv)|*.mkv|All files(*.*)|*.*";

            Song test = new Song();
            if (opnDialogue.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                test.Title = opnDialogue.FileName;
           
            //    string fileName = ;


             test.PlaylistName = PlaylistName;

            PlaylistDB.Playlist.InsertOnSubmit(test);
            PlaylistDB.SubmitChanges();
             */
        }
    }
}

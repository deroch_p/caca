﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Linq.Mapping;
using System.Data.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApplication2
{
    class PlaylistManager : DataContext
    {

        public PlaylistManager(string database) : base(database) {}
        public Table<Song> Playlist;
    }

    [Table(Name = "Songs")]
    public class Song
    {
        [Column(IsPrimaryKey = true)]
        public string Title;
        [Column]
        public string Artist;
        [Column]
        public string Album;
        [Column]
        public string Genre;
        [Column]
        public string PlaylistName;
    }

}
